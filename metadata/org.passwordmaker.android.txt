Categories:Internet
License:GPLv3
Web Site:http://passwordmaker.tasermonkeys.com/android
Source Code:https://github.com/tasermonkey/android-passwordmaker
Issue Tracker:https://github.com/tasermonkey/android-passwordmaker/issues

Auto Name:PasswordMaker Pro
Summary:Password maker
Description:
PasswordMaker Pro creates unique, secure passwords that are very easy for you
to retrieve but no one else. Nothing is stored anywhere, anytime, so there's
nothing to be hacked, lost, or stolen. See http://passwordmaker.org for details.
.

Repo Type:git
Repo:https://github.com/tasermonkey/android-passwordmaker.git

#gson 1.7.1 from https://code.google.com/p/google-gson was processed in this way: \
#https://sites.google.com/site/gson/gson-on-android to produce \
#tasermonkeys 1.7.1 and I can confirm the md5sums match ( using jarjar 1.2)
Build:1.1.7,7
    commit=590e8cf18ec156f28a7c75debf9c76b61b0a77f9
    subdir=PasswordMakerProForAndroid
    target=android-10
    prebuild=mv lib libs

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.7
Current Version Code:7

