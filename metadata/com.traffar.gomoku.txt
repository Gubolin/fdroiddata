Categories:Games
License:GPLv3
Web Site:https://bitbucket.org/przemekr/gomoku
Source Code:https://bitbucket.org/przemekr/gomoku/src
Issue Tracker:https://bitbucket.org/przemekr/gomoku/issues

Auto Name:Gomoku
Summary:Five in a Row Game
Description:
Gomoku is a board strategy game. Two players place in turns black and white
stones on 19x19 square plane. The one who manage to get five in a row first
wins - Like tick-tack-toe on a bigger board.

Play with a friend or against a computer player.

More descriptions and exact rules in the [http://en.wikipedia.org/wiki/Gomoku Wikipedia page].
.

Repo Type:git
Repo:https://bitbucket.org/przemekr/gomoku

Build:0.1,1
    commit=0.1
    subdir=android
    init=cd jni/SDL && \
        rm -rf src include SDL-2.0.1* && \
        wget http://libsdl.org/release/SDL2-2.0.1.tar.gz && \
        tar xf SDL2-2.0.1.tar.gz && \
        ln -s SDL2-2.0.1/src . && \
        ln -s SDL2-2.0.1/include .
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1

