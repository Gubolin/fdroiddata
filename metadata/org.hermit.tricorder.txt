Categories:Games
License:GPLv2
Web Site:https://code.google.com/p/moonblink/wiki/Tricorder
Source Code:https://bitbucket.org/dalb8/tricorder
Issue Tracker:https://code.google.com/p/moonblink/issues

Auto Name:Tricorder
Summary:Star Trek-style gadget
Description:
Turns your Android device into a functional thing like what used
to be used in Star Trek.

Status: Probably the final version, excepting forks.
.

Repo Type:hg
Repo:https://bitbucket.org/dalb8/tricorder

Build:5.12,41
    commit=37a11b9
    subdir=Tricorder
    update=.,../HermitAndroid
    prebuild=rm -rf ../HermitAndroid/src/org/hermit/android/utils && \
        cp -r ../HermitLibrary/src/org/hermit/* ../HermitAndroid/src/org/hermit/android/ && \
        cp -r ../HermitLibrary/src/net ../HermitAndroid/src/ && \
        cp -r ../HermitLibrary/src/ca ../HermitAndroid/src/

Auto Update Mode:None
Update Check Mode:Static
Current Version:5.12
Current Version Code:41

